<?php

namespace Drupal\drupal_coverage_core\Exception;

/**
 * An exception thrown when no connection with TravisCI was possible.
 */
class UnableToDetermineBuildStatusException extends \Exception {}
