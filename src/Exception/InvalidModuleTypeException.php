<?php

namespace Drupal\drupal_coverage_core\Exception;

/**
 * An exception thrown for invalid module types.
 */
class InvalidModuleTypeException extends \Exception {}
